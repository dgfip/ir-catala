module R = Runtime_ocaml.Runtime

type argent = R.money
type declarant = { pensions : argent }
type foyer_fiscal = { declarants : declarant list }

let plancher_abattment = R.money_of_units_int 422
let plafond_abattement = R.money_of_units_int 4123

type detail_resultat_abattement =
  | Abattement of argent
  | SommeAbattement of detail_resultat_abattement list

type resultat_abattement = {
  total : argent;
  detail : detail_resultat_abattement;
}

module CalculNaifDroit = struct
  let abattement_pensions_individuel_deplafonne (d : declarant) : argent =
    let abattement =
      R.Oper.o_mult_mon_rat d.pensions
        (R.Oper.o_div_rat_rat
           {
             filename = "dummy.catala_fr";
             start_line = 0;
             start_column = 0;
             end_line = 0;
             end_column = 0;
             law_headings = [];
           }
           (R.decimal_of_integer (R.integer_of_int 1))
           (R.decimal_of_integer (R.integer_of_int 10)))
    in
    if R.Oper.o_lt_mon_mon abattement plancher_abattment then
      if R.Oper.o_gt_mon_mon plancher_abattment d.pensions then d.pensions
      else plancher_abattment
    else abattement

  let abattement_pensions (foyer : foyer_fiscal) : resultat_abattement =
    let abattement_deplafonne =
      List.fold_left
        (fun acc d ->
          R.Oper.o_add_mon_mon acc (abattement_pensions_individuel_deplafonne d))
        (R.money_of_units_int 0) foyer.declarants
    in
    let abattement_plafonne =
      if R.Oper.o_gt_mon_mon abattement_deplafonne plafond_abattement then
        plafond_abattement
      else R.money_round abattement_deplafonne
    in
    { total = abattement_plafonne; detail = Abattement abattement_plafonne }
end

module CalculPlafondProratisePartoutVersionM2015 = struct
  let abattement_pensions (foyer : foyer_fiscal) =
    let abattements_deplafonnes =
      List.map CalculNaifDroit.abattement_pensions_individuel_deplafonne
        foyer.declarants
    in
    let abattement_total_deplafonne =
      List.fold_left
        (fun acc abattement_deplafonne ->
          R.Oper.o_add_mon_mon acc abattement_deplafonne)
        (R.money_of_units_int 0) abattements_deplafonnes
    in
    let abattements_plafonnes =
      List.map
        (fun abattement_deplafonne ->
          let plafond_proratise =
            if R.Oper.o_eq abattement_total_deplafonne (R.money_of_units_int 0)
            then R.money_of_units_int 0
            else
              R.Oper.o_mult_mon_rat plafond_abattement
                (R.Oper.o_div_mon_mon
                   {
                     filename = "dummy.catala_fr";
                     start_line = 0;
                     start_column = 0;
                     end_line = 0;
                     end_column = 0;
                     law_headings = [];
                   }
                   abattement_deplafonne abattement_total_deplafonne)
          in
          R.money_round
            (if R.Oper.o_gt_mon_mon abattement_deplafonne plafond_proratise then
               plafond_proratise
             else abattement_deplafonne))
        abattements_deplafonnes
    in
    let _, abattements_plafonnes_solde_correct =
      List.fold_left
        (fun (reste_plafond_global, nouveaux_abattements_plafonnes)
             abattement_plafonne ->
          let nouvel_abattement_plafonne =
            if R.Oper.o_gt_mon_mon abattement_plafonne reste_plafond_global then
              reste_plafond_global
            else abattement_plafonne
          in
          ( R.Oper.o_sub_mon_mon reste_plafond_global nouvel_abattement_plafonne,
            nouveaux_abattements_plafonnes @ [nouvel_abattement_plafonne] ))
        (plafond_abattement, []) abattements_plafonnes
    in
    let abattement_total_plafonne =
      List.fold_left
        (fun acc abattement_plafonne ->
          R.Oper.o_add_mon_mon acc abattement_plafonne)
        (R.money_of_units_int 0) abattements_plafonnes_solde_correct
    in
    {
      total = abattement_total_plafonne;
      detail =
        SommeAbattement
          (List.map (fun a -> Abattement a) abattements_plafonnes_solde_correct);
    }
end

module CalculPlafondProratiseNonPlancherVersionM2015 = struct
  let abattement_pensions (foyer : foyer_fiscal) =
    let abattements_deplafonnes =
      List.map
        (fun d ->
          let abattement_deplafonne =
            CalculNaifDroit.abattement_pensions_individuel_deplafonne d
          in
          ( abattement_deplafonne,
            R.Oper.o_eq abattement_deplafonne plancher_abattment
            || R.Oper.o_eq abattement_deplafonne d.pensions ))
        foyer.declarants
    in
    let abattement_total_non_au_plancher, total_abattements_au_plancher =
      List.fold_left
        (fun (abattement_total_non_au_plancher, total_abattements_au_plancher)
             (abattement_deplafonne, au_plancher) ->
          if au_plancher then
            ( abattement_total_non_au_plancher,
              R.Oper.o_add_mon_mon total_abattements_au_plancher
                abattement_deplafonne )
          else
            ( R.Oper.o_add_mon_mon abattement_total_non_au_plancher
                abattement_deplafonne,
              total_abattements_au_plancher ))
        (R.money_of_units_int 0, R.money_of_units_int 0)
        abattements_deplafonnes
    in
    let plafond_a_repartir =
      R.money_round
        (R.Oper.o_sub_mon_mon plafond_abattement total_abattements_au_plancher)
    in
    let abattements_plafonnes =
      List.map
        (fun (abattement_deplafonne, au_plancher) ->
          R.money_round
            (if au_plancher then abattement_deplafonne
             else
               let plafond_proratise =
                 if
                   R.Oper.o_eq abattement_total_non_au_plancher
                     (R.money_of_units_int 0)
                 then R.money_of_units_int 0
                 else
                   R.Oper.o_mult_mon_rat plafond_a_repartir
                     (R.Oper.o_div_mon_mon
                        {
                          filename = "dummy.catala_fr";
                          start_line = 0;
                          start_column = 0;
                          end_line = 0;
                          end_column = 0;
                          law_headings = [];
                        }
                        abattement_deplafonne abattement_total_non_au_plancher)
               in
               if R.Oper.o_gt_mon_mon abattement_deplafonne plafond_proratise
               then plafond_proratise
               else abattement_deplafonne))
        abattements_deplafonnes
    in
    let _, abattements_plafonnes_solde_correct =
      List.fold_left
        (fun (reste_plafond_global, nouveaux_abattements_plafonnes)
             abattement_plafonne ->
          let nouvel_abattement_plafonne =
            if R.Oper.o_gt_mon_mon abattement_plafonne reste_plafond_global then
              reste_plafond_global
            else abattement_plafonne
          in
          ( R.Oper.o_sub_mon_mon reste_plafond_global nouvel_abattement_plafonne,
            nouveaux_abattements_plafonnes @ [nouvel_abattement_plafonne] ))
        (plafond_abattement, []) abattements_plafonnes
    in
    let abattement_total_plafonne =
      List.fold_left
        (fun acc abattement_plafonne ->
          R.Oper.o_add_mon_mon acc abattement_plafonne)
        (R.money_of_units_int 0) abattements_plafonnes_solde_correct
    in
    {
      total = abattement_total_plafonne;
      detail =
        SommeAbattement
          (List.map (fun a -> Abattement a) abattements_plafonnes_solde_correct);
    }
end

module CalculPlafondProratiseNonPlancherVersionM2022 = struct
  let abattement_pensions_individuel_deplafonne (d : declarant) : argent =
    let abattement =
      R.money_round
        (R.Oper.o_mult_mon_rat d.pensions
           (R.Oper.o_div_rat_rat
              {
                filename = "dummy.catala_fr";
                start_line = 0;
                start_column = 0;
                end_line = 0;
                end_column = 0;
                law_headings = [];
              }
              (R.decimal_of_integer (R.integer_of_int 1))
              (R.decimal_of_integer (R.integer_of_int 10))))
    in
    if R.Oper.o_lt_mon_mon abattement plancher_abattment then
      if R.Oper.o_gt_mon_mon plancher_abattment d.pensions then d.pensions
      else plancher_abattment
    else abattement

  let abattement_pensions (foyer : foyer_fiscal) =
    let abattements_deplafonnes =
      List.map
        (fun d ->
          let abattement_deplafonne =
            abattement_pensions_individuel_deplafonne d
          in
          ( abattement_deplafonne,
            R.Oper.o_eq abattement_deplafonne plancher_abattment
            || R.Oper.o_eq abattement_deplafonne d.pensions,
            d.pensions ))
        foyer.declarants
    in
    let total_pensions =
      List.fold_left
        (fun acc d -> R.Oper.o_add_mon_mon acc d.pensions)
        (R.money_of_units_int 0) foyer.declarants
    in
    let abattement_plafonne_global =
      let abattement_deplafonne_global =
        List.fold_left
          (fun acc (a, _, _) -> R.Oper.o_add_mon_mon acc a)
          (R.money_of_units_int 0) abattements_deplafonnes
      in
      if R.Oper.o_gt_mon_mon abattement_deplafonne_global plafond_abattement
      then plafond_abattement
      else abattement_deplafonne_global
    in
    let abattement_total_non_au_plancher, total_abattements_au_plancher =
      List.fold_left
        (fun (abattement_total_non_au_plancher, total_abattements_au_plancher)
             (abattement_deplafonne, au_plancher, _) ->
          if au_plancher then
            ( abattement_total_non_au_plancher,
              R.Oper.o_add_mon_mon total_abattements_au_plancher
                abattement_deplafonne )
          else
            ( R.Oper.o_add_mon_mon abattement_total_non_au_plancher
                abattement_deplafonne,
              total_abattements_au_plancher ))
        (R.money_of_units_int 0, R.money_of_units_int 0)
        abattements_deplafonnes
    in
    let plafond_a_repartir =
      R.money_round
        (R.Oper.o_sub_mon_mon plafond_abattement total_abattements_au_plancher)
    in
    let abattements_plafonnes, _, _ =
      List.fold_left
        (fun ( abattements_plafonnes,
               total_pensions,
               abattements_deplafonnes_traites )
             (abattement_deplafonne, au_plancher, pensions) ->
          let new_total_pensions =
            R.Oper.o_sub_mon_mon total_pensions pensions
          in
          let new_abattements_deplafonnes_traites =
            R.Oper.o_add_mon_mon abattements_deplafonnes_traites
              abattement_deplafonne
          in
          let new_abattements_plafonnes =
            abattements_plafonnes
            @ [
                (if R.Oper.o_gt_mon_mon total_pensions (R.money_of_units_int 0)
                 then
                   R.money_round
                     (if au_plancher then abattement_deplafonne
                      else
                        let plafond_proratise =
                          if
                            R.Oper.o_eq abattement_total_non_au_plancher
                              (R.money_of_units_int 0)
                          then R.money_of_units_int 0
                          else
                            R.Oper.o_mult_mon_rat plafond_a_repartir
                              (R.Oper.o_div_mon_mon
                                 {
                                   filename = "dummy.catala_fr";
                                   start_line = 0;
                                   start_column = 0;
                                   end_line = 0;
                                   end_column = 0;
                                   law_headings = [];
                                 }
                                 abattement_deplafonne
                                 abattement_total_non_au_plancher)
                        in
                        if
                          R.Oper.o_gt_mon_mon abattement_deplafonne
                            plafond_proratise
                        then plafond_proratise
                        else abattement_deplafonne)
                 else
                   let reliquat_abattement_plafonne_global =
                     R.o_sub_mon_mon abattement_plafonne_global
                       abattements_deplafonnes_traites
                   in
                   if
                     R.o_gt_mon_mon reliquat_abattement_plafonne_global
                       (R.money_of_units_int 0)
                   then reliquat_abattement_plafonne_global
                   else R.money_of_units_int 0);
              ]
          in
          ( new_abattements_plafonnes,
            new_total_pensions,
            new_abattements_deplafonnes_traites ))
        ([], total_pensions, R.money_of_units_int 0)
        abattements_deplafonnes
    in
    let abattement_total_plafonne =
      List.fold_left
        (fun acc abattement_plafonne ->
          R.Oper.o_add_mon_mon acc abattement_plafonne)
        (R.money_of_units_int 0) abattements_plafonnes
    in
    {
      total = abattement_total_plafonne;
      detail =
        SommeAbattement (List.map (fun a -> Abattement a) abattements_plafonnes);
    }
end
