# Étude : calcul de l'abattement des pensions, retraites, rentes

Date : 29/09/2023
Auteur : Denis Merigoux

Cet abattement est défini par le 5° a) de l'article 158 du code général
des impôts. Cette étude a pour objectif de comparer les formules
de calcul de cet abattement telles que l'on peut les dériver du droit,
ou telles qu'elles ont été implémentées dans le code M utilisé pour le
calcul de l'impôt sur le revenu.

## Formules de calcul étudiées

### Formule n°1 : selon le droit fiscal

En lisant le 5° a) de l'article 158 du code général des impôts ainsi que les
précisions apportées par le BOFiP (BOI-RSA-PENS-30-10-10), il est possible
d'interpréter le droit pour en déduire cette formule de calcul que nous prendrons
pour référence :

1. pour chaque déclarant, agréger les pensions éligibles pour l'abattement
   et appliquer 10% pour le calculer ;
2. pour chaque déclarant, appliquer le plancher de l'abattement et le maximiser
   par les pensions qui en constituent l'assiette ;
3. agréger les abattements sur tous les déclarants et plafonner cette aggrégation
   au niveau du foyer fiscal, l'arrondir.

### Formule n°2 : selon le code M 2015, version simplifiée

En étudiant le [code M pour le millésime
2015](https://gitlab.adullact.net/dgfip/ir-calcul/-/blob/master/sources2015m_4_6/chap-81.m?ref_type=heads#L297-338),
il est possible de simplifier le calcul utilisé pour en déduire la formule suivante :

1. pour chaque déclarant, agréger les pensions éligibles pour l'abattement
   et appliquer 10% pour le calculer ;
2. pour chaque déclarant, appliquer le plancher de l'abattement et le maximiser
   par les pensions qui en constituent l'assiette ;
3. pour chaque déclarant, plafonner le résultat de l'étape précédente par
   le plafond du foyer fiscal au pro-rata de l'abattement du déclarant par
   rapport aux abattements de tout le foyer fiscal, arrondir ;
4. dans l'ordre des déclarants, plafonner le résultat de l'étape précédente
   par le solde entre le plafond global de l'abattement et les résultats
   traités pour les déclarants précédents ;
5. sommer les résultats de l'étape précédente pour chaque déclarant pour obtenir
   le résultat sur le foyer fiscal.

### Formule n°3 : selon le code M 2015

En étudiant le [code M pour le millésime
2015](https://gitlab.adullact.net/dgfip/ir-calcul/-/blob/master/sources2015m_4_6/chap-81.m?ref_type=heads#L297-338),
on déduit la formule de calcul suivante :

1. pour chaque déclarant, agréger les pensions éligibles pour l'abattement
   et appliquer 10% pour le calculer ;
2. pour chaque déclarant, appliquer le plancher de l'abattement et le maximiser
   par les pensions qui en constituent l'assiette ;
3. pour chaque déclarant dont l'abattement n'est pas au plancher, plafonner le
   résultat de l'étape précédente par le plafond du foyer fiscal au pro-rata de
   l'abattement du déclarant par rapport aux abattements de tout le foyer
   fiscal, arrondir ;
4. dans l'ordre des déclarants, pour ceux dont l'abattement n'est pas au
   plancher, plafonner le résultat de l'étape précédente par le solde entre le
   plafond global de l'abattement et les résultats traités pour les déclarants
   précédents ;
5. sommer les résultats de l'étape précédente pour chaque déclarant pour obtenir
   le résultat sur le foyer fiscal.

### Formule n°4 : selon le code M 2021/2022

En étudiant le [code M pour le millésime
2021](https://gitlab.adullact.net/dgfip/ir-calcul/-/blob/master/sources2021m_5_7/chap-81.m?ref_type=heads#L725-1018),
on déduit la formule de calcul suivante :

1. pour chaque déclarant, agréger les pensions éligibles pour l'abattement
   et appliquer 10% pour le calculer ;
2. pour chaque déclarant, appliquer le plancher de l'abattement et le maximiser
   par les pensions qui en constituent l'assiette ;
3. dans l'ordre des déclarants, deux cas :
   - si les déclarants déjà traités ne représentaient pas l'ensemble des
     pensions du foyer, plafonner le résultat de l'étape précédente par le
     plafond du foyer fiscal au pro-rata de l'abattement du déclarant par
     rapport aux abattements de tout le foyer fiscal, arrondir ;
   - si les déclarants déjà traités représentaient l'ensemble des pensions du
     foyer, distribuer le reliquat de l'abattement global (telle que calculé
     par la formule n°1) non distribué sur les déclarants précédents.
4. sommer les résultats de l'étape précédente pour chaque déclarant pour obtenir
   le résultat sur le foyer fiscal.

Le code M 2021/2022 effectue ensuite une série d'opérations compliquées
visant à pro-ratiser l'abattement individuel pour un déclarant sur
chacune des sources de pensions qui constituent l'assiette de l'abattement.
Cependant, nous ne modéliserons pas cette partie du calcul puisqu'elle ces
abattements pro-ratisés sur chacune des pensions n'apparaissent pas dans
l'avis d'imposition.

## Comparaison des formules de calcul

Nous avons implémenté les 4 formules de calcul ci-dessus dans le langage OCaml
(voir [`abattement_pensions.ml`](./abattement_pensions.ml)). Aussi, nous
pouvons comparer les résultats qu'elles retournent pour un foyer fiscal donné.
À l'aide de l'outil de _property-based testing_ [`qcheck`](https://github.com/c-cube/qcheck/),
nous avons cherché à comparer les formules n°2, 3 et 4 avec la formule n°1, ainsi
que entre elles. Les résultats de ces comparaisons sont les suivantes :

### Correspondance droit fiscal et M 2015 simplifié (n°1 vs n°2)

Les formules ne sont pas strictement équivalentes. Voici un contre-exemple
qui le démontre. Pensions des membres du foyer : 13403.00 € | 19462.00 €.

1. Abattement selon droit fiscal : 3287.00 € (expliqué par 3287.00 €)
2. Abattement selon M 2015 simplifié : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])
3. Abattement selon M 2015 : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])
4. Abattement selon M 2022 : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])

### Correspondance droit fiscal et M 2015 (n°1 vs n°3)

Les formules ne sont pas strictement équivalentes. Voici un contre-exemple qui
le démontre. Pensions des membres du foyer : 13403.00 € | 19462.00 €.

1. Abattement selon droit fiscal : 3287.00 € (expliqué par 3287.00 €)
2. Abattement selon M 2015 simplifié : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])
3. Abattement selon M 2015 : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])
4. Abattement selon M 2022 : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])

### Correspondance droit fiscal et M 2022 (n°1 vs n°4)

Les formules ne sont pas strictement équivalentes. Voici un contre-exemple qui
le démontre.

Les formules ne sont pas strictement équivalentes. Voici un contre-exemple qui
le démontre. Pensions des membres du foyer : 13403.00 € | 19462.00 €.

1. Abattement selon droit fiscal : 3287.00 € (expliqué par 3287.00 €)
2. Abattement selon M 2015 simplifié : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])
3. Abattement selon M 2015 : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])
4. Abattement selon M 2022 : 3286.00 € (expliqué par [1340.00 € | 1946.00 €])

### Correspondance M 2015 simplifié et M 2015 (n°2 vs n°3)

Les formules ne sont pas strictement équivalentes. Voici un contre-exemple qui
le démontre. Pensions des membres du foyer : 55362.00 € | 9335.00 € | 3113.00 €.

1. Abattement selon droit fiscal : 4123.00 € (expliqué par 4123.00 €)
2. Abattement selon M 2015 simplifié : 4122.00 € (expliqué par [3312.00 € | 558.00 € | 252.00 €])
3. Abattement selon M 2015 : 4123.00 € (expliqué par [3167.00 € | 534.00 € | 422.00 €])
4. Abattement selon M 2022 : 4123.00 € (expliqué par [3167.00 € | 534.00 € | 422.00 €])

### Correspondance M 2015 et M 2022 (n°3 vs n°4)

Les formules ne sont pas strictement équivalentes. Voici un contre-exemple qui
le démontre. Pensions des membres du foyer : 45850.00 € | 53735.00 € | 24903.00 €.

1. Abattement selon droit fiscal : 4123.00 € (expliqué par 4123.00 €)
2. Abattement selon M 2015 simplifié : 4123.00 € (expliqué par [1519.00 € | 1780.00 € | 824.00 €])
3. Abattement selon M 2015 : 4123.00 € (expliqué par [1519.00 € | 1780.00 € | 824.00 €])
4. Abattement selon M 2022 : 4124.00 € (expliqué par [1519.00 € | 1780.00 € | 825.00 €])

## Conclusions de l'étude

La pro-ratisation du plafond par déclarant est nécessaire à cause du calcul
des déductions pro-ratisées des revenus au quotient de chaque déclarant.
Il faut donc exclure la méthode n°1. Les méthodes 2 et 3 sont obsolètes
et la distinction des déclarants au plancher ou pas au plancher a déjà
été écartée. Nous choisissons donc la méthode n°4 dans le code M.

Pour ce qui est des problèmes d'arrondis, il y a ici un problème dans
l'implémentation de la méthode 4 puisque le contre-exemple mis en évidence ici
devrait bien coincider avec le droit si on fait le solde inter-déclarant
correctement.
