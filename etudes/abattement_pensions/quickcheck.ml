module R = Runtime_ocaml.Runtime
open Abattement_pensions

let foyer_generator (max_declarants : int) =
  let open QCheck.Gen in
  map
    (fun declarants -> { declarants })
    (list_size
       (int_range 1 max_declarants)
       (map
          (fun pensions -> { pensions = R.money_of_units_int pensions })
          (int_range 0 60000)))

let rec print_detail_abattement (detail : detail_resultat_abattement) : string =
  match detail with
  | Abattement a -> Format.asprintf "%s €" (R.money_to_string a)
  | SommeAbattement sa ->
    "[" ^ String.concat " | " (List.map print_detail_abattement sa) ^ "]"

let print_foyer f =
  let calcul_naif_droit = CalculNaifDroit.abattement_pensions f in
  let calcul_proratise_partout_2015 =
    CalculPlafondProratisePartoutVersionM2015.abattement_pensions f
  in
  let calcul_proratise_non_planchers_2015 =
    CalculPlafondProratiseNonPlancherVersionM2015.abattement_pensions f
  in
  let calcul_proratise_non_planchers_2022 =
    CalculPlafondProratiseNonPlancherVersionM2022.abattement_pensions f
  in
  Format.asprintf
    "Pensions membres du foyer : %a\n\
     Abattement selon droit fiscal : %s € (expliqué par %s) \n\
     Abattement selon M 2015 simplifié : %s € (expliqué par %s)\n\
     Abattement selon M 2015 : %s € (expliqué par %s)\n\
     Abattement selon M 2022 : %s € (expliqué par %s)"
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt " | ")
       (fun fmt d -> Format.fprintf fmt "%s €" (R.money_to_string d.pensions)))
    f.declarants
    (R.money_to_string calcul_naif_droit.total)
    (print_detail_abattement calcul_naif_droit.detail)
    (R.money_to_string calcul_proratise_partout_2015.total)
    (print_detail_abattement calcul_proratise_partout_2015.detail)
    (R.money_to_string calcul_proratise_non_planchers_2015.total)
    (print_detail_abattement calcul_proratise_non_planchers_2015.detail)
    (R.money_to_string calcul_proratise_non_planchers_2022.total)
    (print_detail_abattement calcul_proratise_non_planchers_2022.detail)

let shrink_foyer f =
  QCheck.Iter.map
    (fun declarants -> { declarants })
    (QCheck.Shrink.list f.declarants)

let foyer (max_declarants : int) =
  QCheck.make ~print:print_foyer ~shrink:shrink_foyer
    (foyer_generator max_declarants)

let t1 =
  QCheck.Test.make ~name:"Cohérence version droit fiscal" ~count:100000
    (foyer 6) (fun foyer ->
      let total_pensions =
        List.fold_left
          (fun acc d -> R.Oper.o_add_mon_mon acc d.pensions)
          (R.money_of_units_int 0) foyer.declarants
      in
      let abattement = CalculNaifDroit.abattement_pensions foyer in
      R.Oper.o_gte_mon_mon abattement.total (R.money_of_units_int 0)
      && R.Oper.o_lte_mon_mon abattement.total total_pensions)

let t2 =
  QCheck.Test.make ~name:"Cohérence version M 2015 simplifié" ~count:100000
    (foyer 6) (fun foyer ->
      let total_pensions =
        List.fold_left
          (fun acc d -> R.Oper.o_add_mon_mon acc d.pensions)
          (R.money_of_units_int 0) foyer.declarants
      in
      let abattement =
        CalculPlafondProratisePartoutVersionM2015.abattement_pensions foyer
      in
      R.Oper.o_gte_mon_mon abattement.total (R.money_of_units_int 0)
      && R.Oper.o_lte_mon_mon abattement.total total_pensions)

let t3 =
  QCheck.Test.make ~name:"Cohérence version M 2015" ~count:100000 (foyer 6)
    (fun foyer ->
      let total_pensions =
        List.fold_left
          (fun acc d -> R.Oper.o_add_mon_mon acc d.pensions)
          (R.money_of_units_int 0) foyer.declarants
      in
      let abattement =
        CalculPlafondProratiseNonPlancherVersionM2015.abattement_pensions foyer
      in
      R.Oper.o_gte_mon_mon abattement.total (R.money_of_units_int 0)
      && R.Oper.o_lte_mon_mon abattement.total total_pensions)

let t4 =
  QCheck.Test.make ~name:"Cohérence version M 2012" ~count:100000 (foyer 6)
    (fun foyer ->
      let total_pensions =
        List.fold_left
          (fun acc d -> R.Oper.o_add_mon_mon acc d.pensions)
          (R.money_of_units_int 0) foyer.declarants
      in
      let abattement =
        CalculPlafondProratiseNonPlancherVersionM2022.abattement_pensions foyer
      in
      R.Oper.o_gte_mon_mon abattement.total (R.money_of_units_int 0)
      && R.Oper.o_lte_mon_mon abattement.total total_pensions)

let t5 =
  QCheck.Test.make
    ~name:"Correspondance droit fiscal et M 2015 simplifié (+/- 1€)"
    ~count:1000000 (foyer 3) (fun foyer ->
      let abattement1 = CalculNaifDroit.abattement_pensions foyer in
      let abattement2 =
        CalculPlafondProratisePartoutVersionM2015.abattement_pensions foyer
      in
      Float.abs
        (R.money_to_float
           (R.Oper.o_sub_mon_mon abattement1.total abattement2.total))
      < 2.0)

let t6 =
  QCheck.Test.make ~name:"Correspondance droit fiscal et M 2015 (+/- 1€)"
    ~count:1000000 (foyer 3) (fun foyer ->
      let abattement1 = CalculNaifDroit.abattement_pensions foyer in
      let abattement2 =
        CalculPlafondProratiseNonPlancherVersionM2015.abattement_pensions foyer
      in
      Float.abs
        (R.money_to_float
           (R.Oper.o_sub_mon_mon abattement1.total abattement2.total))
      < 3.0)

let t7 =
  QCheck.Test.make ~name:"Correspondance droit fiscal et M 2022 (+/- 1€)"
    ~count:1000000 (foyer 3) (fun foyer ->
      let abattement1 = CalculNaifDroit.abattement_pensions foyer in
      let abattement2 =
        CalculPlafondProratiseNonPlancherVersionM2022.abattement_pensions foyer
      in
      Float.abs
        (R.money_to_float
           (R.Oper.o_sub_mon_mon abattement1.total abattement2.total))
      < 2.0)

let t8 =
  QCheck.Test.make ~name:"Correspondance M 2015 simplifié et M 2015 (+/- 1€)"
    ~count:1000000 (foyer 3) (fun foyer ->
      let abattement1 =
        CalculPlafondProratisePartoutVersionM2015.abattement_pensions foyer
      in
      let abattement2 =
        CalculPlafondProratiseNonPlancherVersionM2015.abattement_pensions foyer
      in
      Float.abs
        (R.money_to_float
           (R.Oper.o_sub_mon_mon abattement1.total abattement2.total))
      < 2.0)

let t9 =
  QCheck.Test.make ~name:"Correspondance M 2015 et M 2022 (+/- 1€)"
    ~count:1000000 (foyer 3) (fun foyer ->
      let abattement1 =
        CalculPlafondProratiseNonPlancherVersionM2015.abattement_pensions foyer
      in
      let abattement2 =
        CalculPlafondProratiseNonPlancherVersionM2022.abattement_pensions foyer
      in
      Float.abs
        (R.money_to_float
           (R.Oper.o_sub_mon_mon abattement1.total abattement2.total))
      < 2.0)

let t10 =
  QCheck.Test.make ~name:"Correspondance M 2015 et M 2022 (+/- 2€)"
    ~count:1000000 (foyer 3) (fun foyer ->
      let abattement1 =
        CalculPlafondProratiseNonPlancherVersionM2015.abattement_pensions foyer
      in
      let abattement2 =
        CalculPlafondProratiseNonPlancherVersionM2022.abattement_pensions foyer
      in
      Float.abs
        (R.money_to_float
           (R.Oper.o_sub_mon_mon abattement1.total abattement2.total))
      < 3.0)

let () =
  ignore
    (exit
    @@ QCheck_base_runner.run_tests ~verbose:true
         [t1; t2; t3; t4; t5; t6; t7; t8; t9; t10])
