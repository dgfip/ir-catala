module R = Runtime_ocaml.Runtime
module O = Oracles

let prorata_input_generator (max_bases : int) (max_sum : int) =
  let open QCheck.Gen in
  map2
    (fun money_to_distribute bases ->
      {
        O.ProRataArrondiEuro_in.montant_a_distribuer_in =
          R.money_of_units_int money_to_distribute;
        O.ProRataArrondiEuro_in.bases_prorata_in =
          (if
             Array.for_all
               (fun base -> R.o_lte_mon_mon base (R.money_of_units_int 0))
               bases
           then Array.make 0 (R.money_of_units_int 0)
           else bases);
      })
    (int_range 0 max_sum)
    (array_size (int_range 1 max_bases)
       (map
          (fun pensions -> R.money_of_units_int pensions)
          (int_range 0 max_sum)))

let print_prorata_input (i : Oracles.ProRataArrondiEuro_in.t) : string =
  let raw_rounded_proratas =
    Array.map
      R.Oper.(
        fun base ->
          let brut =
            try
              o_mult_rat_rat
                (R.decimal_of_money i.montant_a_distribuer_in)
                (o_div_rat_rat
                   {
                     filename = "dummy.catala_fr";
                     start_line = 0;
                     start_column = 0;
                     end_line = 0;
                     end_column = 0;
                     law_headings = [];
                   }
                   (R.decimal_of_money base)
                   (R.decimal_of_money
                      (Array.fold_left R.Oper.o_add_mon_mon
                         (R.money_of_units_int 0) i.bases_prorata_in)))
            with R.Error (R.DivisionByZero, _) -> R.decimal_of_float 0.0
          in
          R.money_of_decimal @@ R.decimal_round brut)
      i.bases_prorata_in
  in
  let already_distributed =
    Array.fold_left R.Oper.o_add_mon_mon (R.money_of_units_int 0)
      (Array.sub raw_rounded_proratas 0 (Array.length raw_rounded_proratas - 1))
  in
  let raw_rounded_proratas_with_solde = Array.copy raw_rounded_proratas in
  Array.set raw_rounded_proratas_with_solde
    (Array.length raw_rounded_proratas - 1)
    (R.Oper.o_sub_mon_mon i.montant_a_distribuer_in already_distributed);
  Format.asprintf
    "Montant à distribuer : %s\n\
     Bases du prorata : [%a]\n\
     Résultat du prorata : [%a]\n\
     Résultat du prorata avant flips arrondis: [%a]\n\
     Résultat du prorata méthode solde: [%a]"
    (R.money_to_string i.montant_a_distribuer_in)
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
       (fun fmt b -> Format.fprintf fmt "%s €" (R.money_to_string b)))
    (Array.to_list i.bases_prorata_in)
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
       (fun fmt b -> Format.fprintf fmt "%s €" (R.money_to_string b)))
    (Array.to_list (O.pro_rata_arrondi_euro i).valeurs_proratisees)
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
       (fun fmt b -> Format.fprintf fmt "%s €" (R.money_to_string b)))
    (Array.to_list raw_rounded_proratas)
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
       (fun fmt b -> Format.fprintf fmt "%s €" (R.money_to_string b)))
    (Array.to_list raw_rounded_proratas_with_solde)

let shrink_prorata_input i =
  QCheck.Iter.map2
    (fun (bases : int array) (m : int) ->
      {
        O.ProRataArrondiEuro_in.montant_a_distribuer_in = R.money_of_units_int m;
        O.ProRataArrondiEuro_in.bases_prorata_in =
          Array.map R.money_of_units_int bases;
      })
    (QCheck.Shrink.array ~shrink:QCheck.Shrink.int
       (Array.map
          (fun x -> int_of_float (R.money_to_float x))
          i.O.ProRataArrondiEuro_in.bases_prorata_in))
    (QCheck.Shrink.int
       (int_of_float
          (R.money_to_float i.O.ProRataArrondiEuro_in.montant_a_distribuer_in)))

let prorata_input (max_bases : int) (max_sum : int) =
  QCheck.make ~print:print_prorata_input ~shrink:shrink_prorata_input
    (prorata_input_generator max_bases max_sum)

let t1 =
  QCheck.Test.make ~name:"Vérification compte proratisé exact" ~count:100000
    (prorata_input 190 10000000000) (fun prorata_input ->
      let prorata_output = Oracles.pro_rata_arrondi_euro prorata_input in
      R.Oper.o_eq_mon_mon
        (Array.fold_left R.Oper.o_add_mon_mon (R.money_of_units_int 0)
           prorata_output.valeurs_proratisees)
        prorata_input.montant_a_distribuer_in)

let t2 =
  QCheck.Test.make ~name:"Vérification proximité arrondis" ~count:100000
    (prorata_input 200 10000000000) (fun prorata_input ->
      let prorata_output = Oracles.pro_rata_arrondi_euro prorata_input in
      let total_bases =
        Array.fold_left R.Oper.o_add_mon_mon (R.money_of_units_int 0)
          prorata_input.bases_prorata_in
      in
      let raw_rounded_proratas =
        Array.map
          R.Oper.(
            fun base ->
              let brut =
                o_mult_rat_rat
                  (R.decimal_of_money prorata_input.montant_a_distribuer_in)
                  (o_div_rat_rat
                     {
                       filename = "dummy.catala_fr";
                       start_line = 0;
                       start_column = 0;
                       end_line = 0;
                       end_column = 0;
                       law_headings = [];
                     }
                     (R.decimal_of_money base)
                     (R.decimal_of_money total_bases))
              in
              R.money_of_decimal @@ R.decimal_round brut)
          prorata_input.bases_prorata_in
      in
      Array.for_all2
        (fun x y ->
          abs_float (R.money_to_float (R.Oper.o_sub_mon_mon x y)) <= 1.0)
        raw_rounded_proratas prorata_output.valeurs_proratisees)

let t3 =
  QCheck.Test.make
    ~name:"Vérification proximité arrondis (méthode solde dernier élément)"
    ~count:1000000 (prorata_input 40 1000) (fun prorata_input ->
      let total_bases =
        Array.fold_left R.Oper.o_add_mon_mon (R.money_of_units_int 0)
          prorata_input.bases_prorata_in
      in
      let raw_rounded_proratas =
        Array.map
          R.Oper.(
            fun base ->
              let brut =
                try
                  o_mult_rat_rat
                    (R.decimal_of_money prorata_input.montant_a_distribuer_in)
                    (o_div_rat_rat
                       {
                         filename = "dummy.catala_fr";
                         start_line = 0;
                         start_column = 0;
                         end_line = 0;
                         end_column = 0;
                         law_headings = [];
                       }
                       (R.decimal_of_money base)
                       (R.decimal_of_money total_bases))
                with R.Error (R.DivisionByZero, _) -> R.decimal_of_float 0.0
              in
              R.money_of_decimal @@ R.decimal_round brut)
          prorata_input.bases_prorata_in
      in
      let raw_rounded_proratas_with_solde = Array.copy raw_rounded_proratas in
      (match Array.length raw_rounded_proratas with
      | 0 -> ()
      | _ ->
        let already_distributed =
          Array.fold_left R.Oper.o_add_mon_mon (R.money_of_units_int 0)
            (Array.sub raw_rounded_proratas 0
               (Array.length raw_rounded_proratas - 1))
        in
        Array.set raw_rounded_proratas_with_solde
          (Array.length raw_rounded_proratas - 1)
          (R.Oper.o_sub_mon_mon prorata_input.montant_a_distribuer_in
             already_distributed));
      Array.for_all2
        (fun x y ->
          abs_float (R.money_to_float (R.Oper.o_sub_mon_mon x y)) < 10.0)
        raw_rounded_proratas raw_rounded_proratas_with_solde)

let () = ignore (exit @@ QCheck_base_runner.run_tests ~verbose:true [t1; t2; t3])
