# Étude : vérification du calcul de pro-rata.

Date : 18/04/2024
Auteur : Denis Merigoux

Le calcul de prorata avec arrondi utilisé pour le calcul de l'IR est implémenté
de manière externe à Catala et fait donc partie de la base de code de confiance.
Ses propriétés sont les suivantes :
* le montant effectivement réparti au prorata doit rester le même que le montant
  à distribué
* les valeurs pro-ratisées ne doivent pas être éloignées de plus de 1 € des
  valeurs qu'elles auraient eu sans arrondi.

La dernière propriété impose de ne pas faire le solde avec le dernier élément
de la liste mais plutôt de répartir les écarts d'arrondis uniformément sur
tous les éléments.

Cette étude vérifie si l'implémentation OCaml officiel de ce prorata avec
arrondi vérifie bien les deux propriétés ci-dessus.
