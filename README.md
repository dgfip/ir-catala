# Calculette de l'impôt sur le revenu "Catala"

## 🚧 Avertissement

Le contenu de ce dépôt logiciel participe d'une preuve de concept en cours de
réalisation et n'est pas utilisé actuellement dans la production des avis
d'imposition ou tout autre décision prise par la DGFiP. À ce titre, le contenu
de ce dépôt ne saurait être interprété comme reflétant les positions officielles
de la DGFiP sur le plan juridique ou informatique.

⚠️ Si vous cherchez la publication du code source officiel utilisé par la
 DGFiP pour le calcul effectif de l'impôt sur le revenu, rendez-vous sur le
dépôt distinct [`ir-calcul`](https://gitlab.adullact.net/dgfip/ir-calcul).

Le logiciel de calcul de l’impôt sur le revenu, appelé « calculette IR » permet de taxer, tous
les ans, environ 39 millions de foyers fiscaux. Cette calculette IR est
[actuellement programmée en langage M](https://gitlab.adullact.net/dgfip/ir-calcul), langage propre à la DGFiP.

Dans la lignée de [ses efforts d'amélioration et de modernisation du moteur de
calcul de l'impôt sur le
revenu](https://www.inria.fr/fr/mlang-modernisation-calcul-impot-revenu), la
DGFiP a lancé à l'été 2023, en partenariat avec Inria, une expérience de
re-spécification du calcul de l'impôt sur le revenu à l'aide de la technologie
[Catala](https://catala-lang.org/). Ce dépôt héberge le code source produit à
l'occasion de cette expérience.

Le code source de ce dépôt est distribué sous la [licence libre
CeCILL](./LICENSE.md) dont l'article 9.1 dispose :

> Le Licencié reconnaît que l'état actuel des connaissances scientifiques et
> techniques au moment de la mise en circulation du Logiciel ne permet pas d'en
> tester et d'en vérifier toutes les utilisations ni de détecter l'existence
> d'éventuels défauts. L'attention du Licencié a été attirée sur ce point sur
> les risques associés au chargement, à l'utilisation, la modification et/ou au
> développement et à la reproduction du Logiciel qui sont réservés à des
> utilisateurs avertis.
>
> Il relève de la responsabilité du Licencié de contrôler, par tous moyens,
> l'adéquation du produit à ses besoins, son bon fonctionnement et de s'assurer
> qu'il ne causera pas de dommages aux personnes et aux biens.

## 🔍 Description du contenu

Le dépôt contient essentiellement du code source écrit dans le langage de
programmation [Catala](https://catala-lang.org/) dédié à l'implémentation de la
loi fiscale ou sociale. Reportez-vous au [dépôt principal de
Catala](https://github.com/CatalaLang/catala) pour plus d'informations
techniques sur le langage de programmation et les instructions d'utilisation du
compilateur.

Il est ainsi prévu de rapprocher les textes juridiques du code informatique pour
permettre une optimisation des processus métiers entre les services de Gestion
Fiscale (GF) et des Systèmes d'Information (SI) qui passent actuellement par des
documents de spécification intermédiaires. Le langage Catala doit permettre de
justifier chaque étape de calcul par un article de loi (ou disposition
[BOFiP](bofip.impots.gouv.fr/)).

L'objectif à long terme de cette calculette est de calculer le montant de
l'impôt sur le revenu et autres prélèvements obligatoires des particuliers (IFI,
CSG, PSOL, etc.) à partir des données de la déclaration de revenu ([formulaires
2042](https://www.impots.gouv.fr/formulaire/2042/declaration-des-revenus)). Pour
l'instant, le périmètre de l'expérimentation se limite à l'établissement du
revenu brut global au titre de l’imposition des revenus perçus en 2022, sur des
codes de catégorie 1 (traitements, salaires, pensions, rentes) uniquement.

Le dépôt est organisé comme suit :

* Le dossier `sources` contient le code source Catala établi à partir des
  diverses sources juridiques (CGI, lois de finances, BOFiP, etc.)
* Le dossier `tests` contient des cas de tests complètement fictifs créés
  spécifiquement pour les besoins de cette expérience.

## Base de code de confiance

Par défaut, tout le calcul de l'impôt sur le revenu est ici écrit dans le
langage Catala qui est fait pour être auditable et lisible par des juristes.
Cependant, certaines parties du calcul plus compliquées mathématiquement ne
peuvent pas être exprimées en Catala et doivent donc faire l'objet d'une
implémentation externe qui ne pourra être auditée par des juristes. Ces parties
du calcul constituent la "base de code de confiance" (parce qu'on doit lui faire
confiance), qui est très précisément délimitée. Au 25/04/2024, la base de code
de confiance contient :

* le calcul de l'imputation des déficits antérieurs à un revenu déclaré selon
  la règle du plus ancien déficit imputé prioritairement ;
* le calcul d'un pro-rata arrondi à l'euro près avec un mécanisme de solde
  équitable.

Tous les éléments de la base de code de confiance sont rassemblés et exposés
dans le module Catala `Oracles`, et ont été clairement documentés et spécifiés.

## 🏗️ Développement et test

### Commandes globales

Afin d'interagir avec la base de code, il est nécessaire d'installer auparavant
Catala sur sa machine. Veuillez vous référer aux instructions du [dépôt
principal de
Catala](https://github.com/CatalaLang/catala/blob/master/INSTALL.md) à ce
propos. Ici, nous supposerons que vous êtes dans l'un des deux cas suivants :

* soit les binaires `catala` et `clerk` ont été installés et sont accessibles
  depuis le `$PATH` dans votre terminal;
* soit vous utilisez la version de développement de Catala dont le dépôt à été
  cloné sur votre machine dans un dossier décrit par la variable d'environnement
  `$CATALA_DEV_DIR`, ou par défaut à l'emplacement `~/catala`, et vous avez
  compilé les sources dans ce dépôt avec `make build`.

Une fois Catala installé, plusieurs interactions sont possibles avec la base
de code :

* `make typecheck` vérifie l'absence d'erreurs de typage dans la base de
  code du calcul de l'impôt sur le revenu, et affiche les éventuelles erreurs
  et avertissements produits par la vérification du typage.
* `make test` lance les jeux d'essais associés à la base de code du calcul de
  l'impôt sur le revenus présents dans `tests`.
* `make pdf` créé le PDF correspondant à la base de code de calcul de l'impôt
  sur le revenu. Cette interaction nécessite également d'avoir une distribution
  LaTeX installée sur sa machine et l'utilitaire `latexmk`.

### Lancer un test en particulier

Les tests sont définis dans `tests/traitements_salaires.catala_fr` et
`tests/nombre_de_parts.catala_fr`. Chaque test est un champ d'application séparé
dont le nom est `TraitementsSalaires<N>` et `NombreDeParts<N>` avec
`<N>=1,2,etc.`. Pour lancer un test, il suffit d'invoquer :

```
make TraitementsSalaires<N>
```

ou bien

```
make NombreDeParts<N>
```

Pour afficher la trace de l'interprétation du test, il suffit de rajouter
`CATALA_OPTS="-t"` devant `make ...`.
