[@@@warning "-34-37"]

open Catala_utils

(** {2 Model of the values in the test I/O}*)

type value_type = Money | Int | Dec | Bool

type value =
  | Money of int  (** Number of cents *)
  | Int of int
  | Dec of float
  | Bool of bool

(** {2 Model of the 2042 income declaration boxes}*)

type row =
  | Row_ANREV
  | Row_0_A_MDOCVLPFWSG
  | Row_0_B_T
  | Row_0_C_FGHIR
  | Row_0_D_JN
  | Row_R_ABCD_J
  | Row_N_ABCD_J
  | Row_0XX
  | Row_1_ABCD_J
  | Row_1_ABCD_A
  | Row_1_GHIJ_A
  | Row_1P_BCDE
  | Row_1_ABCD_D
  | Row_1_ABCD_V
  | Row_1_GHIJ_B
  | Row_1_GHIJ_F
  | Row_1_ABCD_P
  | Row_1_ABCD_F
  | Row_1_ABCD_G
  | Row_1_ABCD_K
  | Row_1_ABCD_S
  | Row_1_ABCD_T
  | Row_1_ABCD_I
  | Row_1_ABCD_Z
  | Row_1_ABCD_O
  | Row_1_ABCD_L
  | Row_1_ABCD_M
  | Row_1_ABCD_W
  | Row_1_ABCD_R

module Owner = struct
  type t = Filer1 | Filer2 | Dependent1 | Dependent2 | Household

  let compare x y =
    match x, y with
    | Filer1, Filer1
    | Filer2, Filer2
    | Dependent1, Dependent1
    | Dependent2, Dependent2
    | Household, Household ->
      0
    | Household, _ -> 1
    | _, Household -> -1
    | Dependent2, _ -> 1
    | _, Dependent2 -> -1
    | Dependent1, _ -> 1
    | _, Dependent1 -> -1
    | Filer2, _ -> 1
    | _, Filer2 -> -1

  let format fmt x =
    match x with
    | Filer1 -> Format.fprintf fmt "Filer1"
    | Filer2 -> Format.fprintf fmt "Filer2"
    | Dependent1 -> Format.fprintf fmt "Dependent1"
    | Dependent2 -> Format.fprintf fmt "Dependent2"
    | Household -> Format.fprintf fmt "Owner"
end

module OwnerSet = Set.Make (Owner)
module OwnerMap = Map.Make (Owner)

type form_box_data = { row : row; owner : Owner.t; typ : value_type }

module StringMap = Map.Make (String)

type form_boxes = form_box_data StringMap.t

let form_boxes =
  StringMap.of_list
    [
      (* These are variables not in the form but still necessary in the input*)
      "ANREV", { row = Row_ANREV; owner = Household; typ = Int };
      (* Source:
         https://www.impots.gouv.fr/sites/default/files/formulaires/2042/2023/2042_4384.pdf*)
      (* TODO: populate this list automatically from
         https://gitlab.adullact.net/dgfip/ir-calcul/-/blob/master/sources2021m_5_7/tgvI.m
         ? But we don't know which row they are, nor their types or who is the
         owner... *)
      "0AM", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AD", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AO", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AC", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AV", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AL", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AP", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AF", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AW", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AS", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0AG", { row = Row_0_A_MDOCVLPFWSG; owner = Household; typ = Bool };
      "0BT", { row = Row_0_B_T; owner = Household; typ = Bool };
      "0CF", { row = Row_0_C_FGHIR; owner = Household; typ = Int };
      "0CG", { row = Row_0_C_FGHIR; owner = Household; typ = Int };
      "0CH", { row = Row_0_C_FGHIR; owner = Household; typ = Int };
      "0CI", { row = Row_0_C_FGHIR; owner = Household; typ = Int };
      "0CR", { row = Row_0_C_FGHIR; owner = Household; typ = Int };
      "0DJ", { row = Row_0_D_JN; owner = Household; typ = Int };
      "0DN", { row = Row_0_D_JN; owner = Household; typ = Dec };
      "1AJ", { row = Row_1_ABCD_J; owner = Filer1; typ = Money };
      "1BJ", { row = Row_1_ABCD_J; owner = Filer2; typ = Money };
      "1CJ", { row = Row_1_ABCD_J; owner = Dependent1; typ = Money };
      "1DJ", { row = Row_1_ABCD_J; owner = Dependent2; typ = Money };
      "1AA", { row = Row_1_ABCD_A; owner = Filer1; typ = Money };
      "1BA", { row = Row_1_ABCD_A; owner = Filer2; typ = Money };
      "1CA", { row = Row_1_ABCD_A; owner = Dependent1; typ = Money };
      "1DA", { row = Row_1_ABCD_A; owner = Dependent2; typ = Money };
      "1GA", { row = Row_1_GHIJ_A; owner = Filer1; typ = Money };
      "1HA", { row = Row_1_GHIJ_A; owner = Filer2; typ = Money };
      "1IA", { row = Row_1_GHIJ_A; owner = Dependent1; typ = Money };
      "1JA", { row = Row_1_GHIJ_A; owner = Dependent2; typ = Money };
      "1PB", { row = Row_1P_BCDE; owner = Filer1; typ = Money };
      "1PC", { row = Row_1P_BCDE; owner = Filer2; typ = Money };
      "1PD", { row = Row_1P_BCDE; owner = Dependent1; typ = Money };
      "1PE", { row = Row_1P_BCDE; owner = Dependent2; typ = Money };
      "1AD", { row = Row_1_ABCD_D; owner = Filer1; typ = Money };
      "1BD", { row = Row_1_ABCD_D; owner = Filer2; typ = Money };
      "1CD", { row = Row_1_ABCD_D; owner = Dependent1; typ = Money };
      "1DD", { row = Row_1_ABCD_D; owner = Dependent2; typ = Money };
      "1AV", { row = Row_1_ABCD_V; owner = Filer1; typ = Bool };
      "1BV", { row = Row_1_ABCD_V; owner = Filer2; typ = Bool };
      "1CV", { row = Row_1_ABCD_V; owner = Dependent1; typ = Bool };
      "1DV", { row = Row_1_ABCD_V; owner = Dependent2; typ = Bool };
      "1GB", { row = Row_1_GHIJ_B; owner = Filer1; typ = Money };
      "1HB", { row = Row_1_GHIJ_B; owner = Filer2; typ = Money };
      "1IB", { row = Row_1_GHIJ_B; owner = Dependent1; typ = Money };
      "1JB", { row = Row_1_GHIJ_B; owner = Dependent2; typ = Money };
      "1GF", { row = Row_1_GHIJ_F; owner = Filer1; typ = Money };
      "1HF", { row = Row_1_GHIJ_F; owner = Filer2; typ = Money };
      "1IF", { row = Row_1_GHIJ_F; owner = Dependent1; typ = Money };
      "1JF", { row = Row_1_GHIJ_F; owner = Dependent2; typ = Money };
      "1AP", { row = Row_1_ABCD_P; owner = Filer1; typ = Money };
      "1BP", { row = Row_1_ABCD_P; owner = Filer2; typ = Money };
      "1CP", { row = Row_1_ABCD_P; owner = Dependent1; typ = Money };
      "1DP", { row = Row_1_ABCD_P; owner = Dependent2; typ = Money };
      "1AF", { row = Row_1_ABCD_F; owner = Filer1; typ = Money };
      "1BF", { row = Row_1_ABCD_F; owner = Filer2; typ = Money };
      "1CF", { row = Row_1_ABCD_F; owner = Dependent1; typ = Money };
      "1DF", { row = Row_1_ABCD_F; owner = Dependent2; typ = Money };
      "1AK", { row = Row_1_ABCD_K; owner = Filer1; typ = Money };
      "1BK", { row = Row_1_ABCD_K; owner = Filer2; typ = Money };
      "1CK", { row = Row_1_ABCD_K; owner = Dependent1; typ = Money };
      "1DK", { row = Row_1_ABCD_K; owner = Dependent2; typ = Money };
      "1AS", { row = Row_1_ABCD_S; owner = Filer1; typ = Money };
      "1BS", { row = Row_1_ABCD_S; owner = Filer2; typ = Money };
      "1CS", { row = Row_1_ABCD_S; owner = Dependent1; typ = Money };
      "1DS", { row = Row_1_ABCD_S; owner = Dependent2; typ = Money };
      "1AT", { row = Row_1_ABCD_T; owner = Filer1; typ = Money };
      "1BT", { row = Row_1_ABCD_T; owner = Filer2; typ = Money };
      "1CT", { row = Row_1_ABCD_T; owner = Dependent1; typ = Money };
      "1DT", { row = Row_1_ABCD_T; owner = Dependent2; typ = Money };
      "1AI", { row = Row_1_ABCD_I; owner = Filer1; typ = Money };
      "1BI", { row = Row_1_ABCD_I; owner = Filer2; typ = Money };
      "1CI", { row = Row_1_ABCD_I; owner = Dependent1; typ = Money };
      "1DI", { row = Row_1_ABCD_I; owner = Dependent2; typ = Money };
      "1AZ", { row = Row_1_ABCD_Z; owner = Filer1; typ = Money };
      "1BZ", { row = Row_1_ABCD_Z; owner = Filer2; typ = Money };
      "1CZ", { row = Row_1_ABCD_Z; owner = Dependent1; typ = Money };
      "1DZ", { row = Row_1_ABCD_Z; owner = Dependent2; typ = Money };
      "1AO", { row = Row_1_ABCD_O; owner = Filer1; typ = Money };
      "1BO", { row = Row_1_ABCD_O; owner = Filer2; typ = Money };
      "1CO", { row = Row_1_ABCD_O; owner = Dependent1; typ = Money };
      "1DO", { row = Row_1_ABCD_O; owner = Dependent2; typ = Money };
      "1AL", { row = Row_1_ABCD_L; owner = Filer1; typ = Money };
      "1BL", { row = Row_1_ABCD_L; owner = Filer2; typ = Money };
      "1CL", { row = Row_1_ABCD_L; owner = Dependent1; typ = Money };
      "1DL", { row = Row_1_ABCD_L; owner = Dependent2; typ = Money };
      "1AM", { row = Row_1_ABCD_M; owner = Filer1; typ = Money };
      "1BM", { row = Row_1_ABCD_M; owner = Filer2; typ = Money };
      "1CM", { row = Row_1_ABCD_M; owner = Dependent1; typ = Money };
      "1DM", { row = Row_1_ABCD_M; owner = Dependent2; typ = Money };
      "1AW", { row = Row_1_ABCD_W; owner = Household; typ = Money };
      "1BW", { row = Row_1_ABCD_W; owner = Household; typ = Money };
      "1CW", { row = Row_1_ABCD_W; owner = Household; typ = Money };
      "1DW", { row = Row_1_ABCD_W; owner = Household; typ = Money };
      "1AR", { row = Row_1_ABCD_R; owner = Household; typ = Money };
      "1BR", { row = Row_1_ABCD_R; owner = Household; typ = Money };
      "1CR", { row = Row_1_ABCD_R; owner = Household; typ = Money };
      "1DR", { row = Row_1_ABCD_R; owner = Household; typ = Money };
      "0XX", { row = Row_0XX; owner = Household; typ = Money };
      "RAJ", { row = Row_R_ABCD_J; owner = Filer1; typ = Money };
      "RBJ", { row = Row_R_ABCD_J; owner = Filer2; typ = Money };
      "RCJ", { row = Row_R_ABCD_J; owner = Dependent1; typ = Money };
      "RDJ", { row = Row_R_ABCD_J; owner = Dependent2; typ = Money };
      "NAJ", { row = Row_N_ABCD_J; owner = Filer1; typ = Int };
      "NBJ", { row = Row_N_ABCD_J; owner = Filer2; typ = Int };
      "NCJ", { row = Row_N_ABCD_J; owner = Dependent1; typ = Int };
      "NDJ", { row = Row_N_ABCD_J; owner = Dependent2; typ = Int };
    ]

let find_form_box_data (name : string) : form_box_data option =
  StringMap.find_opt name form_boxes

(** {2 Model of a test file input}*)

type raw_test_input_item = {
  variable_name : (string, Pos.t) Mark.ed;
  variable_value : (float, Pos.t) Mark.ed;
}

type raw_test_input = raw_test_input_item list

type resolved_test_input_item = {
  form_box_name : (string, Pos.t) Mark.ed;
  form_box_data : form_box_data;
  form_box_value : value;
}

type resolved_test_input = resolved_test_input_item list

let resolve_test_input (inputs : raw_test_input) : resolved_test_input =
  List.map
    (fun input ->
      let form_box_data =
        match find_form_box_data (Mark.remove input.variable_name) with
        | Some b -> b
        | None ->
          Message.error
            ~pos:(Mark.get input.variable_name)
            "La variable @{<yellow>\"%s\"@} n'existe pas dans les formulaires \
             2042 ou n'a pas été référencée dans cet outil."
            (Mark.remove input.variable_name)
      in
      let form_box_value =
        match form_box_data.typ with
        | Money ->
          let frac_part, int_part =
            Float.modf (Mark.remove input.variable_value)
          in
          if frac_part = 0.0 then Money (Float.to_int int_part * 100)
          else
            Message.error
              ~pos:(Mark.get input.variable_value)
              "La variable @{<yellow>\"%s\"@} attend un nombre entier d'euros, \
               mais le test l'affecte à @{<magenta>%F@}."
              (Mark.remove input.variable_name)
              (Mark.remove input.variable_value)
        | Int ->
          let frac_part, int_part =
            Float.modf (Mark.remove input.variable_value)
          in
          if frac_part = 0.0 then Int (Float.to_int int_part)
          else
            Message.error
              ~pos:(Mark.get input.variable_value)
              "La variable @{<yellow>\"%s\"@} attend un entier, mais le test \
               l'affecte à @{<magenta>%F@}."
              (Mark.remove input.variable_name)
              (Mark.remove input.variable_value)
        | Dec -> Dec (Mark.remove input.variable_value)
        | Bool -> (
          match Mark.remove input.variable_value with
          | 0.0 -> Bool false
          | 1.0 -> Bool true
          | _ ->
            Message.error
              ~pos:(Mark.get input.variable_value)
              "La variable @{<yellow>\"%s\"@} attend la valeur 0 ou 1, mais le \
               test l'affecte à @{<magenta>%F@}."
              (Mark.remove input.variable_name)
              (Mark.remove input.variable_value))
      in
      { form_box_name = input.variable_name; form_box_data; form_box_value })
    inputs

(* TODO: right now this is declared in OCaml but should really be declared in an
   external file with a mini-DSL. *)
let find_catala_variable_correspondance_standard_cases
    (input : resolved_test_input_item) : string option =
  match Mark.remove input.form_box_name, input.form_box_data.row with
  | "ANREV", _ -> None (* this one gets a special treatment *)
  | _, Row_1_ABCD_J -> Some "traitements_salaires"
  | _, Row_1_ABCD_S -> Some "pensions_retraites_rentes"
  | _, Row_1_ABCD_K -> Some "frais_réels"
  | _, Row_1_ABCD_O -> Some "pensions_alimentaires_perçues"
  | _, Row_1_ABCD_Z -> Some "pensions_invalidité"
  | "0AO", _ -> Some "pacsées"
  | "0AM", _ -> Some "mariées"
  | "0AD", _ -> Some "célibataire"
  | "0AC", _ -> Some "divorcée_séparées"
  | "0AV", _ -> Some "veuve"
  | "0AL", _ -> Some "célibataire_divorcé_veuf_sans_enfant"
  | "0AP", _ -> Some "titulaire_carte_invalidité_CMI_invalidité_40_pourcent"
  | "0AF", _ ->
    Some "conjoint_titulaire_carte_invalidité_CMI_invalidité_40_pourcent"
  | "0AW", _ -> Some "pensionné_guerre_célibataire_veuf"
  | "0AS", _ -> Some "pensionné_guerre_marié_pacsé"
  | "0AJ", _ -> Some "pensionné_veuve_de_guerre"
  | "0BT", _ -> Some "parent_isolé"
  | "0XX", _ ->
    None
    (* In the Catala computation, we take the more precise codes for exceptional
       income directly and do not rely on the general one. *)
  | _, (Row_R_ABCD_J | Row_N_ABCD_J) ->
    None (* these will be handled as a special input *)
  | _ ->
    Message.error
      ~pos:(Mark.get input.form_box_name)
      "La variable @{<yellow>\"%s\"@} ne correspond à aucune entrée connue du \
       programme Catala de calcul de l'impôt sur le revenu. Veuillez contacter \
       les mainteneurs de ce harnais de test si vous ne vous êtes pas trompé \
       sur la variable à tester."
      (Mark.remove input.form_box_name)

let format_value fmt (v : value) : unit =
  match v with
  | Int i -> Format.fprintf fmt "%d" i
  | Dec f -> Format.fprintf fmt "%F" f
  | Bool b -> Format.fprintf fmt "%s" (if b then "vrai" else "faux")
  | Money m -> Format.fprintf fmt "%d €" (m / 100)

let format_catala_filer_test_variable_standard_cases
    fmt
    (input : resolved_test_input_item) : unit =
  match find_catala_variable_correspondance_standard_cases input with
  | Some catala_variable ->
    Format.fprintf fmt "@\n  définition %s.%s égal à %a"
      (match input.form_box_data.owner, input.form_box_data.row with
      | Household, (Row_0_A_MDOCVLPFWSG | Row_0_B_T | Row_0_C_FGHIR | Row_0_D_JN)
        ->
        "decription_foyer"
      | Household, (Row_1_ABCD_W | Row_1_ABCD_R) -> "revenus_foyer"
      | Filer1, _ -> "déclarant1"
      | Filer2, _ -> "déclarant2"
      | Dependent1, _ -> "déclarant3"
      | Dependent2, _ -> "déclarant4"
      | Household, _ -> failwith "should not happen")
      catala_variable
      (if input.form_box_data.row = Row_1_ABCD_K then fun fmt v ->
         (* Frais réels are an enum. TODO: where to encode that information
            externally? *)
         Format.fprintf fmt "Oui contenu %a" format_value v
       else format_value)
      input.form_box_value
  | None ->
    () (* we do nothing since there is no corresponding Catala variable. *)

let format_catala_filers_test_variable_nonstandard_cases
    ~(anrev : resolved_test_input_item)
    fmt
    (inputs : resolved_test_input) : unit =
  let exceptional_incomes =
    List.filter
      (fun input ->
        match input.form_box_data.row with
        | Row_R_ABCD_J | Row_N_ABCD_J -> true
        | _ -> false)
      inputs
  in
  let exceptional_incomes =
    OwnerMap.of_list
      (List.map
         (fun owner ->
           ( owner,
             List.filter
               (fun input -> input.form_box_data.owner = owner)
               exceptional_incomes ))
         [Filer1; Filer2; Dependent1; Dependent2])
  in
  OwnerMap.iter
    (fun owner exceptional_incomes ->
      if exceptional_incomes = [] then ()
      else
        let owner_string =
          match owner with
          | Filer1 -> "déclarant1"
          | Filer2 -> "déclarant2"
          | Dependent1 -> "déclarant3"
          | Dependent2 -> "déclarant4"
          | _ -> failwith "should not happen"
        in
        Format.fprintf fmt
          "@\n  définition %s.revenus_exceptionnels_ou_différés égal à [%a]"
          owner_string
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt "")
             (fun fmt exceptional_income ->
               match exceptional_income.form_box_data.row with
               | Row_R_ABCD_J ->
                 let year_number =
                   List.find_opt
                     (fun exceptional_income ->
                       exceptional_income.form_box_data.row = Row_N_ABCD_J)
                     exceptional_incomes
                 in
                 Format.fprintf fmt
                   "RevenuExceptionnelOuDifféré {@.    -- valeur : %a@.    -- \
                    régime : Article163_0_A@.    -- échéance : %s@.    -- \
                    catégorie : TraitementsSalaires }"
                   format_value exceptional_income.form_box_value
                   (match year_number with
                   | Some year_number -> (
                     match year_number.form_box_value, anrev.form_box_value with
                     | Int i, Int j ->
                       "RevenuDifféréÉchéanceNormale contenu "
                       ^ string_of_int (j - i)
                     | _ -> failwith "should not happen")
                   | None -> "RevenuExceptionnel")
               | _ -> ()))
          exceptional_incomes)
    exceptional_incomes

let input_has_owner owner inputs =
  List.exists (fun i -> i.form_box_data.owner = owner) inputs

(* TODO: right now the master scopes to be called in Catala is are
   "TraitementsSalairesFoyerFiscal" and "NombreDeParts but that may change as
   the code gets bigger and bigger."*)
let format_catala_test_input
    (interface_file : string)
    (ir_source_file : string)
    fmt
    (inputs : resolved_test_input) : unit =
  let anrev =
    List.find
      (fun i -> String.equal (Mark.remove i.form_box_name) "ANREV")
      inputs
  in
  Format.fprintf fmt
    {catala_fr|> Inclusion: %s
> Inclusion: %s

# Test

```catala
déclaration structure Résultat:
  donnée parts contenu NombreDeParts
  donnée traitements_salaires contenu TraitementsSalairesFoyerFiscal

déclaration champ d'application Test:
  decription_foyer champ d'application InterfaceDescriptionFoyerFiscal
  revenus_foyer champ d'application InterfaceTraitementsSalairesFoyerFiscal
  déclarant1 champ d'application InterfaceTraitementsSalairesDéclarant
%s%s%s
  résultat sortie contenu Résultat

champ d'application Test:%a%a
  définition sortie égal à Résultat {
    -- traitements_salaires : (résultat de TraitementsSalairesFoyerFiscal avec {
      -- déclarant1 : déclarant1.sortie
      -- déclarant2 : %s
      -- déclarations_personnes_à_charge : [%s]
      -- année_revenu : %a
      -- revenus : revenus_foyer.sortie
    })
    -- parts : (résultat de NombreDeParts avec {
      -- foyer_fiscal: decription_foyer.sortie
      -- année_revenu: %a
    })
  }
```|catala_fr}
    ir_source_file interface_file
    (if input_has_owner Filer2 inputs then
       "  déclarant2 champ d'application InterfaceTraitementsSalairesDéclarant\n"
     else "")
    (if input_has_owner Dependent1 inputs then
       "  déclarant3 champ d'application InterfaceTraitementsSalairesDéclarant\n"
     else "")
    (if input_has_owner Dependent2 inputs then
       "  déclarant4 champ d'application InterfaceTraitementsSalairesDéclarant\n"
     else "")
    (Format.pp_print_list
       ~pp_sep:(fun fmt () -> Format.fprintf fmt "")
       format_catala_filer_test_variable_standard_cases)
    inputs
    (format_catala_filers_test_variable_nonstandard_cases ~anrev)
    inputs
    (if input_has_owner Filer2 inputs then
       "Déclaration contenu déclarant2.sortie"
     else "PasDeDéclaration")
    (match
       input_has_owner Dependent1 inputs, input_has_owner Dependent2 inputs
     with
    | true, true -> "déclarant3.sortie, déclarant4.sortie"
    | false, true -> "déclarant4.sortie"
    | true, false -> "déclarant3.sortie"
    | false, false -> "")
    format_value anrev.form_box_value format_value anrev.form_box_value

type ir_result_item = { name : string; value : value }
type ir_result = ir_result_item list

(* TODO: Right now, the rules of correspondance between the Catala output and
   the M output variables is hand-coded in this OCaml function. But actually we
   should make a mini-DSL to write this correspondance in an external file that
   is then parsed an interpreted here. *)
let interpret_catala_result
    (people_in_household : OwnerSet.t)
    (catala_result :
      ( (Shared_ast.yes, Shared_ast.no, Shared_ast.yes) Shared_ast.interpr_kind,
        'm )
      Shared_ast.gexpr) : ir_result =
  let retrieve_money_variable_from_expr e (m_variable_name : string) =
    match Mark.remove e with
    | Shared_ast.ELit (LMoney m) ->
      {
        name = m_variable_name;
        value =
          Money
            (Runtime_ocaml.Runtime.integer_to_int
               (Runtime_ocaml.Runtime.money_to_cents m));
      }
    | _ -> failwith "should not happen"
  in
  let retrieve_decimal_variable_from_expr e (m_variable_name : string) =
    match Mark.remove e with
    | Shared_ast.ELit (LRat d) ->
      {
        name = m_variable_name;
        value = Dec (Runtime_ocaml.Runtime.decimal_to_float d);
      }
    | _ -> failwith "should not happen"
  in
  let index_to_people (index : int) : Owner.t =
    let ordered_list_of_people_in_household =
      List.sort Owner.compare (OwnerSet.elements people_in_household)
    in
    List.nth ordered_list_of_people_in_household index
  in
  let struct_name_equal name str =
    String.equal (Mark.remove (Shared_ast.StructName.get_info name)) str
  in
  let match_struct s_name fields_func e =
    match Mark.remove e with
    | Shared_ast.EStruct { name; fields } when struct_name_equal name s_name ->
      fields_func fields
    | _ -> failwith "Should not happen"
  in
  let match_struct_fold_fields e s_name fields_fold_func acc =
    match_struct s_name
      (fun fields ->
        Shared_ast.StructField.Map.fold fields_fold_func fields acc)
      e
  in
  let match_field_by_name field_name_cases field_name field acc =
    match
      List.assoc_opt
        (Mark.remove (Shared_ast.StructField.get_info field_name))
        field_name_cases
    with
    | None -> acc
    | Some field_name_case -> field_name_case field acc
  in
  let match_struct_fold_fields_by_name s_name field_name_cases e acc =
    match_struct_fold_fields e s_name (match_field_by_name field_name_cases) acc
  in
  let match_array e array_func acc =
    match Mark.remove e with
    | Shared_ast.EArray array -> array_func array acc
    | _ -> failwith "should not happen"
  in
  let match_array_fold_values array_value_func e acc =
    match_array e
      (fun array acc ->
        snd
          (List.fold_left
             (fun (index, acc) elt ->
               let new_acc = array_value_func index elt acc in
               index + 1, new_acc)
             (0, acc) array))
      acc
  in
  let match_array_map_values array_elt_func e acc =
    match Mark.remove e with
    | Shared_ast.EArray elements ->
      List.flatten
        (List.map (fun element -> array_elt_func element []) elements)
      @ acc
    | _ -> failwith "should not happen"
  in
  let retrieve_field_value retrieve_func field_name field acc =
    retrieve_func field field_name :: acc
  in
  let retrieve_struct_field fields field_name =
    Mark.remove
      (snd
         (Shared_ast.StructField.Map.find_first
            (fun field_name' ->
              String.equal
                (Mark.remove (Shared_ast.StructField.get_info field_name'))
                field_name)
            fields))
  in
  let retrieve_enum_constructor_name_in_struct_field fields field_name =
    match retrieve_struct_field fields field_name with
    | Shared_ast.EInj { name = _; e = _; cons } ->
      Mark.remove (Shared_ast.EnumConstructor.get_info cons)
    | _ -> failwith "should not happen"
  in
  let l =
    match_struct_fold_fields_by_name "Résultat"
      [
        ( "traitements_salaires",
          match_struct_fold_fields_by_name "TraitementsSalairesFoyerFiscal"
            [
              ( "revenu_brut_global",
                retrieve_field_value retrieve_money_variable_from_expr "TSPR" );
              ( "rentes_viagères_titre_onéreux",
                retrieve_field_value retrieve_money_variable_from_expr "RVTOT" );
              ( "déclarations_avec_résultats_traitements_salaires",
                match_array_fold_values (fun index ->
                    match_struct_fold_fields_by_name
                      "TraitementsSalairesDéclarant"
                      [
                        ( "traitements_salaires",
                          retrieve_field_value retrieve_money_variable_from_expr
                            (match index_to_people index with
                            | Filer1 -> "TSBV"
                            | Filer2 -> "TSBC"
                            | Dependent1 -> "TSB1"
                            | Dependent2 -> "TSB2"
                            | _ -> failwith "should not happen") );
                        ( "déduction_frais_professionnels_traitements_salaires",
                          retrieve_field_value retrieve_money_variable_from_expr
                            (match index_to_people index with
                            | Filer1 -> "FPTV"
                            | Filer2 -> "FPTC"
                            | Dependent1 -> "FPT1"
                            | Dependent2 -> "FPT2"
                            | _ -> failwith "should not happen") );
                        ( "abattement_pensions_retraites_rentes",
                          retrieve_field_value retrieve_money_variable_from_expr
                            (match index_to_people index with
                            | Filer1 -> "ABPRV"
                            | Filer2 -> "ABPRC"
                            | Dependent1 -> "ABPR1"
                            | Dependent2 -> "FPT2"
                            | _ -> failwith "should not happen") );
                        ( "revenus_quotientés",
                          match_array_map_values
                            (match_struct "RevenuQuotienté" (fun fields ->
                                 let quotiented_income_category =
                                   retrieve_enum_constructor_name_in_struct_field
                                     fields "catégorie"
                                 in
                                 Shared_ast.StructField.Map.fold
                                   (fun field_name field acc ->
                                     match
                                       ( Mark.remove
                                           (Shared_ast.StructField.get_info
                                              field_name),
                                         quotiented_income_category )
                                     with
                                     | "déduction", "TraitementsSalaires" ->
                                       retrieve_field_value
                                         retrieve_money_variable_from_expr
                                         (match index_to_people index with
                                         | Filer1 -> "TABTSRAJ"
                                         | Filer2 -> "TABTSRBJ"
                                         | Dependent1 -> "TABTSRCJ"
                                         | Dependent2 -> "TABTSRDJ"
                                         | _ -> failwith "should not happen")
                                         field acc
                                     | "valeur_nette", "TraitementsSalaires" ->
                                       retrieve_field_value
                                         retrieve_money_variable_from_expr
                                         (match index_to_people index with
                                         | Filer1 -> "TTSNRAJ"
                                         | Filer2 -> "TTSNRBJ"
                                         | Dependent1 -> "TTSNRCJ"
                                         | Dependent2 -> "TTSNRDJ"
                                         | _ -> failwith "should not happen")
                                         field acc
                                     | _ -> acc)
                                   fields)) );
                      ]) );
            ] );
        ( "parts",
          match_struct_fold_fields_by_name "NombreDeParts"
            [
              ( "nombre_de_parts",
                retrieve_field_value retrieve_decimal_variable_from_expr "NBPT"
              );
              ( "abattement",
                retrieve_field_value retrieve_money_variable_from_expr "ABTMA" );
            ] );
      ]
      catala_result []
  in
  List.rev l

let autotest_fip
    (interface_file : string)
    (ir_source_file : string)
    (trace : bool)
    (debug : bool) =
  try
    let raw_test_input =
      (* This should be parsed from a standard FIP DGFiP test file. *)
      [
        {
          variable_name = "ANREV", Pos.no_pos;
          variable_value = 2022.0, Pos.no_pos;
        };
        {
          variable_name = "1AJ", Pos.no_pos;
          variable_value = 72360.0, Pos.no_pos;
        };
        {
          variable_name = "1BJ", Pos.no_pos;
          variable_value = 46544.0, Pos.no_pos;
        };
        {
          variable_name = "1CZ", Pos.no_pos;
          variable_value = 6936.0, Pos.no_pos;
        };
        {
          variable_name = "1BK", Pos.no_pos;
          variable_value = 5000.0, Pos.no_pos;
        };
        {
          variable_name = "0XX", Pos.no_pos;
          variable_value = 4123.0, Pos.no_pos;
        };
        {
          variable_name = "RAJ", Pos.no_pos;
          variable_value = 4123.0, Pos.no_pos;
        };
        { variable_name = "NAJ", Pos.no_pos; variable_value = 4.0, Pos.no_pos };
        { variable_name = "0AM", Pos.no_pos; variable_value = 1.0, Pos.no_pos };
      ]
    in
    let resolved_test_input = resolve_test_input raw_test_input in
    if
      not
        (List.exists
           (fun i -> String.equal (Mark.remove i.form_box_name) "ANREV")
           resolved_test_input)
    then
      Message.error
        "Le cas de test doit contenir la valeur de la variable \
         @{<yellow>\"ANREV\"@} qui indique l'année du revenu taxé.";
    let people_in_household =
      List.fold_left
        (fun people_in_household people ->
          if input_has_owner people resolved_test_input then
            OwnerSet.add people people_in_household
          else people_in_household)
        OwnerSet.empty
        [Filer1; Filer2; Dependent1; Dependent2]
    in
    let test_contents =
      Format.asprintf "%a"
        (format_catala_test_input interface_file ir_source_file)
        resolved_test_input
    in
    let ppf = Format.formatter_of_out_channel stdout in
    Ocolor_format.prettify_formatter ppf;
    if debug then
      Format.fprintf ppf
        "@{<magenta;bold>[DEBUG]@} Test translated to Catala:@\n\
         @[<hov 2>%s@]@\n"
        test_contents;
    let options =
      Global.enforce_options ~disable_warnings:true ~trace
        ~language:(Some Global.Fr)
        ~input_src:(Contents (test_contents, "test.catala_fr"))
        ~debug ()
    in
    let program, _execution_order =
      Driver.Passes.dcalc options ~includes:[] ~optimize:false
        ~check_invariants:false ~typed:Shared_ast.Expr.typed
    in
    let test_scope = Driver.Commands.get_scope_uid program.decl_ctx "Test" in
    let catala_results =
      Shared_ast.Interpreter.interpret_program_dcalc program test_scope
    in
    if debug then
      Format.fprintf ppf
        "@{<magenta;bold>[DEBUG]@} Catala output:@\n@[<hov 2>%a@]@\n"
        (Shared_ast.Print.UserFacing.value Global.Fr)
        (snd (List.hd catala_results));
    Format.fprintf ppf
      "@{<green;bold>[SUCCÈS]@} @{<green>Le test s'est exécuté sans erreur !@}@\n";
    Format.fprintf ppf
      "@{<blue;bold>[ENTRÉES]@} @{<blue>Rappel des entrées du test :@}@\n%a@\n"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt "@\n")
         (fun fmt input ->
           Format.fprintf fmt "@{<bold>%s@}/@{<yellow>%a@}"
             (Mark.remove input.form_box_name)
             format_value input.form_box_value))
      resolved_test_input;
    let ir_result =
      interpret_catala_result people_in_household (snd (List.hd catala_results))
    in
    Format.fprintf ppf
      "@{<cyan;bold>[RÉSULTATS]@} @{<cyan>Les résultats sont :@}@\n%a\n"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt "@\n")
         (fun fmt result ->
           Format.fprintf fmt "@{<bold>%s@}/@{<yellow>%a@}" result.name
             format_value result.value))
      ir_result
  with Message.CompilerError content ->
    Format.pp_print_flush Format.std_formatter ();
    Message.Content.emit content Message.Error

open Cmdliner

let catala_ir_interface_file =
  let doc =
    "Chemin vers le fichier $(interface.catala_fr) qui contient les champs \
     d'application d'interface pour les tests."
  in
  Arg.(
    required
    & opt (some string) None
    & info ["i"; "interface"] ~docv:"INTERFACE" ~doc)

let catala_ir_source_file =
  let doc =
    "Chemin vers le fichier $(impot_revenu.catala_fr) qui contient le code \
     source de la calculette IR Catala."
  in
  Arg.(
    required
    & opt (some string) None
    & info ["s"; "source_impot_revenu"] ~docv:"IMPOT_REVENU" ~doc)

let trace =
  Arg.(
    value
    & flag
    & info ["trace"; "t"]
        ~doc:"Affiche la trace d'exécution du programme Catala")

let debug =
  Arg.(
    value
    & flag
    & info ["debug"; "d"]
        ~env:(Cmd.Env.info "CATALA_DEBUG")
        ~doc:"Prints debug information.")

let autotest_fip_t =
  Term.(
    const autotest_fip
    $ catala_ir_interface_file
    $ catala_ir_source_file
    $ trace
    $ debug)

let cmd =
  let doc = "Exécute un test au format FIP avec la calculette IR Catala" in
  let man =
    [
      `S Manpage.s_bugs;
      `P
        "En cas de problème, soumettez un ticket sur \
         <https://gitlab.adullact.net/dgfip/ir-catala/-/issues>.";
    ]
  in
  let info = Cmd.info "autotest_fip" ~version:"v0.1.0" ~doc ~man in
  Cmd.v info autotest_fip_t

let () = exit (Cmd.eval cmd)
