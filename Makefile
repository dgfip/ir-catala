CATALA_DEV_DIR ?= $(HOME)/catala

CATALA_DEV_BIN ?= $(CATALA_DEV_DIR)/_build/install/default/bin/catala
CATALA_BIN ?= $(if $(wildcard $(CATALA_DEV_BIN)),$(CATALA_DEV_BIN),catala)

CLERK_DEV_BIN ?= $(CATALA_DEV_DIR)/_build/install/default/bin/clerk
CLERK_BIN ?= $(if $(wildcard $(CLERK_DEV_BIN)),$(CLERK_DEV_BIN),clerk)

CLERK_OPTS += -I sources $(if $(CATALA_OPTS),--catala-opts="$(CATALA_OPTS)",)

typecheck:
	$(CATALA_BIN) Typecheck $(CATALA_OPTS) sources/impot_revenu.catala_fr

build: sources/impot_revenu.cmxs

ALWAYS:

# Building to a catala module
%.cmxs: ALWAYS
	$(CLERK_BIN) build $(CLERK_OPTS) _build/$@

test: ALWAYS
	$(CLERK_BIN) test $(CLERK_OPTS) tests

test-native-%:
	$(CLERK_BIN) build $(CLERK_OPTS) _build/tests/$*.exe
	@if ./_build/tests/$*.exe; then \
	  echo "Native run of $* successful"; \
	else \
	  echo "Native test of $* failed"; exit 1; \
	fi

test-native: test-native-nombre_de_parts test-native-traitements_salaires

%.tex: %.catala_fr
	$(CATALA_BIN) LaTeX $(CATALA_OPTS) $*.catala_fr -w

%.pdf: %.tex
	cd $(<D) && latexmk -xelatex -shell-escape -halt-on-error $(<F)

LITERATE = sources/impot_revenu.catala_fr sources/oracles.catala_fr tests/interface.catala_fr tests/nombre_de_parts.catala_fr tests/traitements_salaires.catala_fr

latex: impot_revenu.tex
pdf: impot_revenu.pdf

impot_revenu.tex: $(LITERATE)
	$(CATALA_BIN) LaTeX $(CATALA_OPTS) $^ -w -o $@

TraitementsSalaires%:
	$(CLERK_BIN) run $(CLERK_OPTS) \
		tests/traitements_salaires.catala_fr -s $@

BénéficesNonCommerciaux%:
	$(CLERK_BIN) run $(CLERK_OPTS) \
		tests/benefices_non_commerciaux.catala_fr -s $@

BénéficesIndustrielsCommerciaux%:
	$(CLERK_BIN) run $(CLERK_OPTS) \
		tests/benefices_industriels_commerciaux.catala_fr -s $@


NombreDeParts%:
	$(CLERK_BIN) run $(CLERK_OPTS) \
		tests/nombre_de_parts.catala_fr -s $@

clean:
	@rm -rf _build
	@for d in . sources tests; do \
	  cd "$d" && rm -f *.tex *.aux *.fdb_latexmk *.fls *.log *.out *.toc *.xdv; \
	done

distclean: clean
	@rm -f *.pdf sources/*.pdf tests/*.pdf

.PHONY: clean distclean
